package Week11.assignment.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import Week11.assignment.app.entities.Book;
import Week11.assignment.app.entities.LoginUser;
import Week11.assignment.app.service.BookService;
import Week11.assignment.app.service.LoginUserService;

@RestController
public class AdminController {


	@Autowired
	private LoginUserService loginUserService;
	
	@Autowired
	private BookService bookService;

	@Autowired
	private RestTemplate template;

	//**********************************Book Operations***********************************
	
//	@RequestMapping("/books")
//	public boolean getBooks(Map<String, List<Book>> map) {

//		String url = "http://USER-SERVICE/book";

//		BookList adminBookList =  this.template.getForObject(url, BookList.class);
//		System.out.println(adminBookList.getListOfBooks());

//		map.put("listOfAllBooks", adminBookList.getListOfBooks());

//		if(adminBookList.getListOfBooks().isEmpty()) {
			//			session.setAttribute("mssg", "List is Empty");
//			System.out.println("List is empty");
//			return false;
//		}else {
//			for(Book book : adminBookList.getListOfBooks()) {
//				System.out.println(book);
//			}
//			System.out.println("Login page");
//			return true;
//		}
//	}

	@PostMapping("/registered")
	public boolean registerUserDetails(@RequestBody LoginUser loginUser) {

		boolean isInserted = this.loginUserService.registerUser(loginUser);
		System.out.println(isInserted);

		if(isInserted) {
			System.out.println(loginUser);
			return true;
		}else {
			return false;
		}

	}

	@PostMapping("/users")
	public LoginUser getUserDetails(@RequestBody LoginUser loginUser) throws Exception {

		LoginUser user = this.loginUserService.findUser(loginUser);

		System.out.println(user);

		return user;
	}
	
	@PostMapping("/add")
	public boolean addBook(@RequestBody Book book) {
		
		return this.bookService.addBookDetails(book);
	}
	
	@PostMapping("/update")
	public boolean updateBook(@RequestBody Book book) {
		
		return this.bookService.updateBook(book);
	}
	
	@PostMapping("/delete")
	public boolean deleteBook(@RequestBody Book book) {
		
		return this.bookService.deleteBook(book.getBookId());
	}
	
	@GetMapping("/book")
	public ResponseEntity<List<Book>> getAllBooks() {
		
		List<Book> listOfBooks = this.bookService.getAllBooks();
		
		return new ResponseEntity<List<Book>>(listOfBooks,HttpStatus.OK);
	}
	
	
	
	//******************************User Operations*********************************
	
	@PostMapping("/adduser")
	public boolean addUser(@RequestBody LoginUser loginUser) {
		
		return this.loginUserService.registerUser(loginUser);
	}
	
	@PostMapping("/deleteuser")
	public boolean deleteUser(@RequestBody LoginUser loginUser) {
		return this.loginUserService.deleteUser(loginUser);
	}
	
	@PostMapping("/updateuser")
	public boolean updateUser(@RequestBody LoginUser loginUser) {
		return this.loginUserService.updateUser(loginUser);
	}
	
	@GetMapping("/allusers")
	public ResponseEntity<List<LoginUser>> getAllUsers() {
		
		List<LoginUser> listOfUsers = this.loginUserService.getAllUsers();
		
		return new ResponseEntity<List<LoginUser>>(listOfUsers,HttpStatus.OK);
	}
}
