package Week11.assignment.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Week11.assignment.app.entities.LoginUser;
import Week11.assignment.app.repositories.LoginUserRepository;

@Service
public class LoginUserService {
	
	@Autowired
	private LoginUserRepository loginUserRepository;
	
	public boolean registerUser(LoginUser loginUser) {
		System.out.println(loginUser.getEmail());
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			return false;
		}
		this.loginUserRepository.save(loginUser);
		return true;
	}
	
	public LoginUser findUser(LoginUser loginUser) throws Exception {
		System.out.println("LoginUser is " + loginUser);
		System.out.println(this.loginUserRepository.findById(loginUser.getEmail()).get());
		return this.loginUserRepository.findById(loginUser.getEmail()).get();
	}
	
	public boolean deleteUser(LoginUser loginUser) {
		System.out.println(loginUser.getEmail());
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			this.loginUserRepository.deleteById(loginUser.getEmail());
			return true;
		}
		return false;
	}

	public boolean updateUser(LoginUser loginUser) {
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			this.loginUserRepository.save(loginUser);
			return true;
		}
		return false;
	}

	public List<LoginUser> getAllUsers() {
		return (List<LoginUser>)this.loginUserRepository.findByType("user");
	}
}
