package week11.assignment.app.controllers;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import week11.assignment.app.entities.LoginUser;

@Controller
public class LoginUserController {
	
	@Autowired
	private RestTemplate template ;
	
	@GetMapping("/register")
	public String getRegisterPage() {
		System.out.println("getRegisterPage() is called");
		return "register";
	}
	
	@GetMapping("/login")
	public String getLoginPage() {
		System.out.println("getLoginPage() is called");
		return "login";
	}
	
	@PostMapping("/getRegistered")
	public String getUserRegistered(LoginUser loginUser,HttpSession session) {
		
		System.out.println("getUserRegistered() is called");
		
		if(loginUser.getEmail().isEmpty() && loginUser.getName().isEmpty() && loginUser.getPassword().isEmpty()) {
			session.setAttribute("mssg", "Fields can not be empty");
			return "redirect:/message";
		}
		
		if(loginUser.getType().equals("admin") && loginUser.getEmail().endsWith("hcl.com")) {
			
			String url = "http://ADMIN-SERVICE/registered";
			
			Boolean isInserted  = this.template.postForObject(url, loginUser, Boolean.class);
			
			if(isInserted) {
				return "/login";
			}else {
				System.out.println("Invalid Credentials");
				session.setAttribute("mssg", "Invalid Credentials");
				return "redirect:/message";
			}
			
		}else{
			
			System.out.println("You are not an Admin");
			session.setAttribute("mssg", "You are not an Admin");
			return "redirect:/message";
			
		}
		
	}

	

	@PostMapping("/dashboard")
	public String getDashboard(LoginUser loginUser,HttpSession session) {
		
		System.out.println("before if");
		try {
			
			if(loginUser.getEmail().isEmpty() && loginUser.getPassword().isEmpty() && loginUser.getType().isEmpty()) {

				System.out.println("Your Fields are Empty");
				session.setAttribute("mssg", "Your Fields are Empty");
				return "redirect:/message";

			}else {
				
				System.out.println("after if");
				
				String url = "http://ADMIN-SERVICE/users";
				
				LoginUser user = this.template.postForObject(url,loginUser, LoginUser.class);
				
				
				if(user.getEmail().equalsIgnoreCase(loginUser.getEmail()) 
						&& user.getPassword().equalsIgnoreCase(loginUser.getPassword()))
				{
					session.setAttribute("email",user.getEmail());
					session.setAttribute("name", user.getName());
					session.setAttribute("type", user.getType());
					return "/dashboard";

				}else {

					System.out.println("Your Credentials Are Wrong");
					session.setAttribute("mssg", "Your Credentials Are Wrong");
					return "redirect:/message";
				}
			}
			
		}catch (Exception e) {
			
			System.out.println(e.getMessage());
			session.setAttribute("mssg", "Your Fields can not be Empty");
			return "redirect:/message";
		}
	}

		
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		System.out.println("LoginUser logout() is called");

		session.removeAttribute("email");
		session.removeAttribute("name");
		session.removeAttribute("type");
		session.invalidate();

		return "redirect:/login";
	}
	
	@GetMapping("/dashboard")
	public String getDashboard() {
		System.out.println("getRegisterPage() is called");
		return "dashboard";
	}
}
