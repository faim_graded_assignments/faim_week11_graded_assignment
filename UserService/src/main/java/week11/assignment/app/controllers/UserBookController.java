package week11.assignment.app.controllers;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import week11.assignment.app.entities.Book;
import week11.assignment.app.entities.LoginUser;


@Controller
public class UserBookController {

	@Autowired
	private RestTemplate template;


	//***********************************************Book Operations****************************************
	@GetMapping("/book")
	public String getAllBooks(HttpSession session) {

		String url = "http://ADMIN-SERVICE/book";

		ResponseEntity<Book[]> listOfBooks = this.template.getForEntity(url, Book[].class);

		List<Book> listOfAllBooks = Arrays.asList(listOfBooks.getBody());

		if(listOfAllBooks.isEmpty()) {
			session.setAttribute("mssg", "List Of Books is Empty");
			return "redirect:/message";
		}else {
			session.setAttribute("list", listOfAllBooks);
			System.out.println(listOfAllBooks);
			return "book";
		}
	}

	
	@GetMapping("/add")
	public String addBook() {
		System.out.println("Admin Page method addBook() called");
		return "add";
	}

	
	// Method for adding new book 
	
	@PostMapping("/addbook")
	public String addBook(Book book,HttpSession session) {
		System.out.println("Book Controller addBook() is called");

		String url = "http://ADMIN-SERVICE/add";

		Boolean isAdded = this.template.postForObject(url, book, Boolean.class);

		//		boolean isAdded = this.bookService.addBookDetails(book);

		if(isAdded) {
			session.setAttribute("mssg", "Book with Name " + book.getBookName() + " Successfully Added");
			return "redirect:/message";
		}else {
			System.out.println("Boook is Already present in Database");
			return "redirect:/message";
		}
	}


	@GetMapping("/delete")
	public String deleteBook() {
		System.out.println("deleteBook() from Book Controller is called");
		return "delete";
	}


	// Method for deleting a book
	@PostMapping("/deletebook")
	public String deleteBookById(@RequestParam("bookId") int id,HttpSession session) {
		System.out.println("deleteBookById() from BookController is called");

		String url = "http://ADMIN-SERVICE/delete";

		Book book = new Book();
		book.setBookId(id);

		Boolean isDeleted = this.template.postForObject(url, book, Boolean.class);

		//		boolean isDeleted = this.bookService.deleteBook(id);

		if(isDeleted) {
			session.setAttribute("mssg", "Book With Id " + id + " has been Deleted");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "Book With id " + id + " Does not Exists");
			return "redirect:/message";
		}
	}
	
	
	// Getting id of the book for updation
	
	@GetMapping("/edit/{bookId}")
	public String getBookForUpdation(@PathVariable int bookId,HttpSession session) {
		
		System.out.println("getBookForUpdation() is from BookController called");
		
		session.setAttribute("bookId", bookId);
		return "edit";
	}

	
	
	// Method for updation
	@PostMapping("/update")
	public String updateBookById(Book book,HttpSession session) {
		System.out.println("updateBookById() from BookController is called");

		String url = "http://ADMIN-SERVICE/update";

		Boolean isUpdated = this.template.postForObject(url, book, Boolean.class);

		System.out.println(book);

		if(book.getBookName().isEmpty() && book.getBookGenre().isEmpty()) {
			session.setAttribute("mssg", "Fields Can not Empty");
			return "redirect:/message";
		}

		//		boolean isUpdated = this.bookService.updateBook(book);

		if(isUpdated) {
			session.setAttribute("mssg", "Book With Id " + book.getBookId() + " has been Updated");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "Book With id " + book.getBookId() + " Does not Exists");
			return "redirect:/message";
		}
	}



	//***********************************************User Operations****************************************

	
	
	@GetMapping("/addnewuser")
	public String getAddUserPage() {
		System.out.println("addUser() is called");
		return "adduser";
	}
	
	@GetMapping("/allusers")
	public String getAllUsers(HttpSession session) {
		
		System.out.println("getAllUsers() is called");
		
		String url = "http://ADMIN-SERVICE/allusers";

		ResponseEntity<LoginUser[]> listOfUsers = this.template.getForEntity(url, LoginUser[].class);

		List<LoginUser> listOfAllUsers = Arrays.asList(listOfUsers.getBody());

		if(listOfAllUsers.isEmpty()) {
			session.setAttribute("mssg", "List Of Users is Empty");
			return "redirect:/message";
		}else {
			session.setAttribute("users", listOfAllUsers);
			System.out.println(listOfAllUsers);
			return "users";
		}

	}
	
	@GetMapping("/deletetheuser")
	public String getDeleteUserPage() {
		System.out.println("getDeleteUserPage() is called");
		return "/deleteuser";
	}

	@GetMapping("/updatetheuser/{email}")
	public String getUpdateUserPage(@PathVariable String email, HttpSession session) {
		System.out.println("getUpdateUserPage() is called");
		session.setAttribute("userEmail", email);
		return "/edituser";
	}

	
	// Method for adding a new user done by admin
	
	@PostMapping("/adduser")
	public String addUser(LoginUser loginUser,HttpSession session) {

		System.out.println("addUser() is called");

		String url = "http://ADMIN-SERVICE/adduser";
		loginUser.setType("user");

		Boolean isUserAdded = this.template.postForObject(url, loginUser, Boolean.class);

		if(isUserAdded) {
			session.setAttribute("mssg", "User Added Successfully");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "User Already Registered");
			return "redirect:/message";
		}

	}
	
	//  Deleting a user
	
	@PostMapping("/deleteuser")
	public String deleteUser(LoginUser loginUser,HttpSession session) {

		System.out.println("deleteUser() is called");

		String url = "http://ADMIN-SERVICE/deleteuser";

		Boolean isUserDeleted = this.template.postForObject(url, loginUser, Boolean.class);

		if(isUserDeleted) {
			session.setAttribute("mssg", "User Deleted Successfully");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "User is not Registered");
			return "redirect:/message";
		}

	}
	
	
	// Updating user details
	
	@PostMapping("/updateuser")
	public String updateUser(@RequestParam("email") String email,HttpSession session) {

		System.out.println("updateUser() is called");

		String url = "http://ADMIN-SERVICE/updateuser";
		
		LoginUser loginUser = new LoginUser();
		loginUser.setEmail(email);

		Boolean isUserUpdated = this.template.postForObject(url, loginUser, Boolean.class);

		if(isUserUpdated) {
			session.setAttribute("mssg", "User Updated Successfully");
			return "redirect:/message";
		}else {
			session.setAttribute("mssg", "User is not Registered");
			return "redirect:/message";
		}

	}
}
