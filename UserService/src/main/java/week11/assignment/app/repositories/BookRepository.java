package week11.assignment.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import week11.assignment.app.entities.Book;

@Repository
public interface BookRepository extends CrudRepository<Book,Integer>{
	
}
