package week11.assignment.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import week11.assignment.app.entities.Liked;
import week11.assignment.app.repositories.LikedRepository;


@Service
public class LikedBookService {

	@Autowired
	private LikedRepository likedRepository;

	public List<Liked> getAllLikedBooks() {
		List<Liked> listOfLikedBooks = new ArrayList<>();
		this.likedRepository.findAll().forEach((likedBooks -> listOfLikedBooks.add(likedBooks)));
		return listOfLikedBooks;
	}

	public boolean addToLiked(Liked liked) {
		if(this.likedRepository.existsById(liked.getBookId())) {
			return false;
		}
		this.likedRepository.save(liked);
		return true;
	}

	public boolean deleteBook(int id) {
		if(this.likedRepository.existsById(id)) {
			this.likedRepository.deleteById(id);
			return true;
		}
		return false;
	}
	
}
