<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>

<%
		String email = (String)session.getAttribute("email");
		String name = (String) session.getAttribute("name");
		String type = (String) session.getAttribute("type");
		String message = (String)session.getAttribute("mssg");
	%>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<style>
body {
	background-image: linear-gradient(to right, red, yellow);
}

.header {
	background-color: black;
	color: white;
	height: 100px;
	margin-top: -7px;
	margin-left: -15px;
	width: 101%;
}

.title {
	font-family: Arial, Helvetica, sans-serif;
	display: inline-block;
	text-align: center;
	margin-top: 15px;
	font-size: 40px;
	margin-left: 15px;
	height: 50%;
	padding: 5px;
}

<%
if(email != null)
{
%>
.navLink {
	display: inline-block;
	margin-left: 42%;
}
<%
}else{
%>
.navLink {
	display: inline-block;
	margin-left: 70%;
}
<%
}
%>
.navUL {
	list-style-type: none;
}

li {
	display: inline;
}

a {
	color: white;
	text-decoration: none;
}

.li {
	margin-left: 10px;
}
.mark {
	font-size: 40px;
	padding: 5px;
	margin: 10px;
	background-image: linear-gradient(to right, yellow, purple);
	border-radius: 10px;
	color: white;
}
</style>
<body>
	
	<div class="header" id="headerId">
		<div class="title">Bookess</div>
		<div class="navLink">
			<ul class="navUL">
				<%
                	if(email != null && type.equals("user"))
                	{
                %><li class="li"><a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="book" class="login"><button
							class="btn btn-primary">Show All Books</button></a></li>
				<li class="li"><a href="like" class="login"><button
							class="btn btn-primary">Liked Books</button></a></li>
				<li class="li"><a href="read" class="login"><button
							class="btn btn-primary">Read Later Books</button></a></li>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%
				}else if(email != null && type.equals("admin")){
                %>
                <li class="li"><a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="book" class="login"><button
							class="btn btn-primary">Show All Books</button></a></li>
				<li class="li"><a href="add" class="login"><button
							class="btn btn-primary">Add Book</button></a></li>
				<li class="li"><a href="delete" class="login"><button
							class="btn btn-primary">Delete Book</button></a></li>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%}else{ %>
				<li class="li"><a href="login" class="login"><button
							class="btn btn-primary">Login</button></a></li>
				<%} %>
			</ul>
		</div>
	</div>
	<%
		if(email != null)
		{
	%>
	<marquee class="mark" scrollamount="15">
		Welcome <strong class="name" title="Your Name"><%=name%></strong> to
		Bookess => Choose Option from the Navigation bar
	</marquee>
	<%}else{ %>
	<marquee class="mark" scrollamount="15"> Welcome to Bookess
		=> Choose Option from the Navigation bar</marquee>
	<%}%>
	<marquee class="mark" scrollamount="20"><%=message%></marquee>
</body>
</html>